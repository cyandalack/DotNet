USE [ShoeDatabase]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([id], [name]) VALUES (1, N'Men')
INSERT [dbo].[Category] ([id], [name]) VALUES (2, N'Woman')
INSERT [dbo].[Category] ([id], [name]) VALUES (3, N'Kids')
SET IDENTITY_INSERT [dbo].[Category] OFF
SET IDENTITY_INSERT [dbo].[Type] ON 

INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (1, N'Joggers', 1)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (2, N'Football', 1)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (3, N'Cricket', 1)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (4, N'Tennis', 1)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (5, N'Casual', 1)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (6, N'Tops', 2)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (7, N'Bottoms', 2)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (8, N'Yoga', 2)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (9, N'Sports', 2)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (10, N'Gym', 2)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (11, N'Tees', 3)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (12, N'Shorts', 3)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (13, N'Gear', 3)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (14, N'Watches', 3)
INSERT [dbo].[Type] ([id], [name], [categoryId]) VALUES (15, N'Shoes', 3)
SET IDENTITY_INSERT [dbo].[Type] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([id], [username], [password], [email], [isAdmin], [address]) VALUES (1, N'tangtuan', N'123456', N'tangtuan.uit@gmail.com', 0, N'HCM')
INSERT [dbo].[User] ([id], [username], [password], [email], [isAdmin], [address]) VALUES (2, N'vantien', N'123456', N'vantien@gmail.com', 0, N'HCM')
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([id], [userId], [date]) VALUES (1, 1, NULL)
INSERT [dbo].[Order] ([id], [userId], [date]) VALUES (3, 2, NULL)
SET IDENTITY_INSERT [dbo].[Order] OFF
SET IDENTITY_INSERT [dbo].[Produce] ON 

INSERT [dbo].[Produce] ([id], [name], [description]) VALUES (1, N'Nice', N'NIKE, Inc. fosters a culture of invention. We create products, services and experiences for today’s athlete while solving problems for the next generation.')
INSERT [dbo].[Produce] ([id], [name], [description]) VALUES (2, N'Adidas', N'Adidas AG (stylised as adidas since 1949) is a German multinational corporation, headquartered in Herzogenaurach, Germany')
INSERT [dbo].[Produce] ([id], [name], [description]) VALUES (3, N'Puma', N'PUMA and Right To Play Germany Launch Partnership – Cooperation aims to develop full potential of children and youth through the power of sports and play.')
SET IDENTITY_INSERT [dbo].[Produce] OFF
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (1, N'Nike Air Max', 150.0000, N'Internal bootie wraps your foot for a sock-like fit
Unique eyestays work with the Flywire cables to create an even better glove-like fit
Waffle outsole for durability and multi-surface traction
Sculpted Cushlon midsole combines plush cushioning and springy resilience for impact protection
Midsole flex grooves for greater forefoot flexibility', 4, N'show.jpg,show1.jpg,show2.jpg,show3.jpg', 1, 1)
INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (2, N'MESSI 16+ PUREAGILITY FIRM GROUND BOOTS', 250.0000, N'Messi''s domination of the game is surpassed by no other. Where others are fast, he is first. Strive for the same superhuman acceleration and untouchable agility in these elite-level juniors'' football boots. Featuring a light, snug-fitting AGILITYKNIT upper that fuels the type of play that wins matches. Designed to win on firm ground.', 4, N'messi1.jpg,messi2.jpg,messi3.jpg,messi4.jpg', 2, 2)
INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (4, N'NIKE MERCURIAL VICTORY VI DYNAMIC FIT CR7 FG', 159.0000, N'The Nike Mercurial Victory VI Dynamic Fit CR7 Firm-Ground Football Boot features a sleek upper and an innovative stud arrangement for incredible touch and traction on short-grass pitches.', 5, N'nike-mercurial1.jpg,nike-mercurial2.jpg,nike-mercurial3.jpg,nike-mercurial4.jpg', 1, 2)
INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (5, N'X FOOTBALL SHOES', 100.0000, N'Modelled after adult football boots, these kids'' shoes combine standout performance and style. The synthetic upper is light and durable and features a large performance logo. Adjustable hook-and-loop straps allow a fast, easy fit.', 3, N'XFOOTBALLSHOES1.jpg,XFOOTBALLSHOES2.jpg,XFOOTBALLSHOES3.jpg,XFOOTBALLSHOES4.jpg', 2, 15)
INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (6, N'IGNITE 3 PWRCOOL WOMEN’S RUNNING SHOES', 110.0000, N'Feel cooler and run faster in the all-new IGNITE 3 PWRCOOL. This cushioning shoe offers supreme energy return and breathability to make your runs even more fun and comfortable. The upper is equipped with open, deconstructed, super breathable mesh upper for ultimate airflow, combined with a TPU heel piece that will lock your foot perfectly onto the platform. With a faster Chevron design and a lightweight feel, the shoe''s tooling delivers an increased energy return for an even smoother ride on hot summer days.', 4, N'IGNITE3PWRCOOLWOMEN1.jpg,IGNITE3PWRCOOLWOMEN2.jpg,IGNITE3PWRCOOLWOMEN3.jpg,IGNITE3PWRCOOLWOMEN4.jpg', 3, 9)
INSERT [dbo].[Product] ([id], [name], [price], [detail], [rating], [image], [produceId], [typeId]) VALUES (7, N'TSUGI SHINSEI MEN’S TRAINING SHOES', 100.0000, N'"Tsugi" means "next" in Japanese, and the Tsugi Shinsei is ready for whatever''s next and whatever comes its way. Inspired by Japanese architecture, its progressive design features IGNITE FOAM for cushioning, a unique lace construction, and a fully knitted sock. It''s next level streetwear for next level gamechangers.', 3, N'TSUGISHINSEIMEN1.jpg,TSUGISHINSEIMEN2.jpg,TSUGISHINSEIMEN3.jpg,TSUGISHINSEIMEN4.jpg', 3, 3)
SET IDENTITY_INSERT [dbo].[Product] OFF
SET IDENTITY_INSERT [dbo].[Size] ON 

INSERT [dbo].[Size] ([id], [name]) VALUES (1, N'S')
INSERT [dbo].[Size] ([id], [name]) VALUES (2, N'M')
INSERT [dbo].[Size] ([id], [name]) VALUES (3, N'L')
SET IDENTITY_INSERT [dbo].[Size] OFF
INSERT [dbo].[OrderDetail] ([orderId], [productId], [quantity], [size]) VALUES (1, 1, 2, 1)
INSERT [dbo].[OrderDetail] ([orderId], [productId], [quantity], [size]) VALUES (1, 2, 1, 2)
INSERT [dbo].[OrderDetail] ([orderId], [productId], [quantity], [size]) VALUES (3, 4, 1, 2)
INSERT [dbo].[OrderDetail] ([orderId], [productId], [quantity], [size]) VALUES (3, 5, 1, 3)
