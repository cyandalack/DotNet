namespace indentity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUserLogins", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUserRoles", "UserId", "dbo.ApplicationUser");
            DropPrimaryKey("dbo.ApplicationUser");
            AlterColumn("dbo.ApplicationUser", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.ApplicationUser", "Email", c => c.String(maxLength: 256));
            AddPrimaryKey("dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUserLogins", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUserRoles", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationUserRoles", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ApplicationUserLogins", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.ApplicationUser");
            DropPrimaryKey("dbo.ApplicationUser");
            AlterColumn("dbo.ApplicationUser", "Email", c => c.String());
            AlterColumn("dbo.ApplicationUser", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.ApplicationUserRoles", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ApplicationUserLogins", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: true);
        }
    }
}
