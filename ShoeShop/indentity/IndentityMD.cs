﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
namespace indentity
{
    public class ApplicationUser : IdentityUser
    {
        //public int id { get; set; }
        //public string email { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public string address { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("name=ShoesDBConnectionString")
        {
        }
    }
}
