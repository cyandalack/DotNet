﻿﻿using ShoeShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShoeShop.Controllers
{
    public class OrderDetailController : Controller
    {

        // GET: /OrderDetail/Create
        [HttpGet]
        public ActionResult Create(int product_id, int qty, int size)
        {
            try {
                ShoeDatabaseEntities db = new ShoeDatabaseEntities();
                User current_user = db.Users.Where(c => c.username == User.Identity.Name).First();
                var order = new Order();
                if (current_user.Orders.Count() > 0)
                    order = current_user.Orders.Last();
                else
                {
                    order = db.Orders.Add(new Order() { userId = current_user.id, date = DateTime.Now });
                    db.SaveChanges();
                }
                db.OrderDetails.Add(new OrderDetail() { orderId = order.id, productId = product_id, quantity = qty, size = 1 });
                db.SaveChanges();
            }
            catch (Exception e)
            {
                
            }
            
            return RedirectToAction("CheckOut", "Order");
        }

        [HttpPost]
        public ActionResult AddOrderDetail(int product_id, int qty, int size)
        {
            try
            {
                ShoeDatabaseEntities db = new ShoeDatabaseEntities();
                User current_user = db.Users.Where(c => c.username == User.Identity.Name).First();
                var order = new Order();
                if (current_user.Orders.Count() > 0)
                    order = current_user.Orders.Last();
                else
                {
                    order = db.Orders.Add(new Order() { userId = current_user.id, date = DateTime.Now });
                    db.SaveChanges();
                }
                db.OrderDetails.Add(new OrderDetail() { orderId = order.id, productId = product_id, quantity = qty, size = 1 });
                db.SaveChanges();
                int? price = 0;
                foreach (OrderDetail item in order.OrderDetails)
                {
                    Product product = db.Products.Find(item.productId);
                    price += Convert.ToInt32(product.price) * item.quantity;
                };

                OrderDetailTotal result = new OrderDetailTotal() { count = order.OrderDetails.Count(), total = price };

                return Json(result);
            }
            catch (Exception e)
            {
                return new HttpStatusCodeResult(400, "Bad request");
            }
            
            
            
        }

        public ActionResult Remove(int? oId, int? pId)
        {
            ShoeDatabaseEntities db = new ShoeDatabaseEntities();
            OrderDetail od = db.OrderDetails.Where(c => c.orderId == oId && c.productId == pId).FirstOrDefault();
            db.OrderDetails.Remove(od);
            db.SaveChanges();
            return RedirectToAction("CheckOut", "Order");
        }

        public ActionResult PlacOrder(int? id)
        {
            ShoeDatabaseEntities db = new ShoeDatabaseEntities();
            Order order = db.Orders.Find(id);

            db.OrderDetails.RemoveRange(order.OrderDetails);
            db.SaveChanges();
            return RedirectToAction("CheckOut", "Order");
        }
    }
}
