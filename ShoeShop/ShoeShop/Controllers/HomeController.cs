﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoeShop.Models;
using Microsoft.AspNet.Identity;
using indentity;
namespace ShoeShop.Controllers
{
    public class HomeController : Controller
    {
        private ShoeDatabaseEntities db;
        protected MyContext ApplicationDbContext { get; set; }

        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        protected UserManager<indentity.ApplicationUser> UserManager { get; set; }
        public ActionResult Index()
        {
            db = new ShoeDatabaseEntities();
            var model = db.Products;
            return View(model);
        }

        //public ActionResult LoadProduct()
        //{
        //    db = new ShoeDatabaseEntities();
        //    var model = db.Products;
        //    return PartialView("_ShowProduct",model);
        //}

        public ActionResult SearchProduct(String text)
        {
            db = new ShoeDatabaseEntities();
            var model = from m in db.Products
                        where m.name.Contains(text)
                        select m;
            return View("Index", model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            var user = UserManager.FindById(User.Identity.GetUserId());
            return View();
        }
    }
}