﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoeShop.Models;

namespace ShoeShop.Controllers
{
    public class CategoiesController : Controller
    {
        private ShoeDatabaseEntities dc;

        public ActionResult Index(int type)
        {
            dc = new ShoeDatabaseEntities();
            var model = dc.Types.Where(t => t.id == type).First();
            return View(model);
        }

        // GET: Categoies
        public ActionResult Category()
        {
            dc = new ShoeDatabaseEntities();
            var model = dc.Categories;

            var order = new Order();
            int? price = 0;
            if (User.Identity.IsAuthenticated)
            {
                User current_user = dc.Users.Where(c => c.username == User.Identity.Name).First();
                if (current_user.Orders.Count() > 0)
                    order = current_user.Orders.Last();
                else
                {
                    order = dc.Orders.Add(new Order() { userId = current_user.id, date = DateTime.Now });
                    dc.SaveChanges();
                }
                foreach (OrderDetail item in order.OrderDetails)
                {
                    Product product = dc.Products.Find(item.productId);
                    price += Convert.ToInt32(product.price) * item.quantity;
                };

            }
            OrderDetailTotal result = new OrderDetailTotal() { count = order.OrderDetails.Count(), total = price };

            ViewBag.result = result;
            return PartialView("_Header", model);
        }

        public ActionResult GetAllType()
        {
            dc = new ShoeDatabaseEntities();
            var model = dc.Categories;
            return PartialView("_Type", model);
        }

        public ActionResult GetAllBrand()
        {
            dc = new ShoeDatabaseEntities();
            var model = dc.Produces;
            return PartialView("_Brand", model);
        }

        public ActionResult ProductWithType(int type, int page = 1, int limit = 12)
        {
            dc = new ShoeDatabaseEntities();
            var model = dc.Products.Where(p => p.typeId == type);
            var productsPaging = model.OrderBy(p => p.name).Skip((page - 1) * limit).Take(limit);
            return PartialView("_ProductWithType", productsPaging);
        }
    }
}