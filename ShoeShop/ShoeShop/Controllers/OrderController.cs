﻿﻿using ShoeShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShoeShop.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CheckOut()
        {
            ShoeDatabaseEntities db = new ShoeDatabaseEntities();
            User current_user = db.Users.Where(c => c.username == User.Identity.Name).First();
            var order = new Order();
            if (current_user.Orders.Count() > 0)
                order = current_user.Orders.Last();
            else
            {
                order = db.Orders.Add(new Order() { userId = current_user.id, date = DateTime.Now });
                db.SaveChanges();
            }
            int? price = 0;
            foreach (OrderDetail item in order.OrderDetails)
            {
                item.Size1 = db.Sizes.Find(item.size);
                item.Product = db.Products.Find(item.productId);
                price += Convert.ToInt32(item.Product.price) * item.quantity;
            };
            ViewBag.order_details = order.OrderDetails;
            ViewBag.total_price = price;
            ViewBag.delivery_charges = 150;
            ViewBag.orderID = order.id;
            ViewBag.number_record = order.OrderDetails.Count();
            return View();
        }
    }
}