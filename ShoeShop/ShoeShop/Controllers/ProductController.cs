﻿﻿using ShoeShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShoeShop.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ShoeDatabaseEntities db = new ShoeDatabaseEntities();
            Product item = db.Products.Find(id);
            ViewBag.Message = item;
            if (item == null)
            {
                return HttpNotFound();
            }
            var similar_item = db.Products.Where(c => c.typeId == item.typeId).ToList();
            ViewBag.Message2 = similar_item;

            var list_sizes = db.Sizes;
            ViewBag.list_sizes = list_sizes;
            return View();

        }
    }
}
